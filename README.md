﻿# .Net Challenge 

The goal of this project is to create a simple browser-based chat application using .NET.  
 
This application should allow several users to talk in a chatroom and also to get stock quotes from an API using a specific command. 

## Features

- Register
- Login
- Users can talk with other users in a chatroom
- Users can post messages as commands into the chatroom with ```/stock=stock_code```
- Chat messages ordered by their timestamps and show only the last 50 messages.
- More than one chatroom.

## Requeriments

- Microsoft Visual Studio Community 2019
- SQL Server 2017 Express LocalDB 

## Installation

The easiest way to test is to run the project in visual studio, to avoid having to create a server in IIS and mount the database to the sql server.

So clone, open the project and run.

In case you want to use a publication you can find one in "bin/Release/Publish"

## Bonus 

- .NET identity for users authentication
- Handled messages that are not understood or any exceptions raised within the bot. 
- ~~Build an installer~~: I would be happy if you could share with me how this can be done.