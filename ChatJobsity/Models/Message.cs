﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChatJobsity.Models
{
    public class Message
    {
        public Message()
        {
            Created = DateTime.Now;
        }
        public int ID { get; set; }
        [ForeignKey("Room")]
        public int? RoomID { get; set; }
        [ForeignKey("Participant")]
        public int? ParticipantID { get; set; }
        public DateTime Created { get; set; }
        public String Value { get; set; }
        public String CreatedPretty
        {
            get
            {
                String date = Created.ToString("yyyy-MM-ddTHH:mm:ss");
                return date;
            }
        }

        public String ParticipantPretty
        {
            get
            {
                string name = Participant != null ? Participant.User.UserName : "Deleted";
                return name;
            }
        }

        public virtual Room Room { get; set; }
        public virtual Participant Participant { get; set; }
    }
}