﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatJobsity.Models
{
    public class Room
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }
    }
}