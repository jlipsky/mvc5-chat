﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChatJobsity.Models
{
    public class Participant
    {
        public int ID { get; set; }
        public int? RoomID { get; set; }
        [ForeignKey("User")]
        public String UserID { get; set; }

        public virtual Room Room { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}