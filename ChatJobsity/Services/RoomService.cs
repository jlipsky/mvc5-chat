﻿using ChatJobsity.DAL;
using ChatJobsity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace ChatJobsity.Services
{
    public class RoomService
    {
        ChatJobsityDbContext db;
        public RoomService(ChatJobsityDbContext db)
        {
            this.db = db;
        }
        public Participant Connect(String userId, Room room)
        {
            Participant participant = room.Participants.FirstOrDefault<Participant>(x => x.RoomID == room.ID && x.User.Id == userId);

            if (participant == null)
            {
                participant = db.Participants.Create<Participant>();
                participant.Room = room;
                participant.UserID = userId;
                room.Participants.Add(participant);
                db.SaveChanges();
            }

            return participant;
        }

        public static int LeaveRoom(String userId, int roomId)
        {
            int changes = 0;
            using (var db = new ChatJobsityDbContext())
            {
                IQueryable<Participant> query = db.Participants.Include(i => i.Messages);
                Participant participant = query.FirstOrDefault(x => x.User.Id == userId && x.RoomID == roomId);
                db.Participants.Remove(participant);
                try
                {
                    changes = db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
            }
            return changes;
        }

        public Message Message(int roomId, Participant participant, String message)
        {
            Message messageObj = db.Messages.Create();
            messageObj.RoomID = roomId;
            messageObj.Participant = participant;
            messageObj.Value = message;
            db.Messages.Add(messageObj);
            if(db.SaveChanges() > 0)
            {
                return messageObj;
            }
                else
            {
                throw new Exception("Error creating message");
            }
            
        }

    }
}