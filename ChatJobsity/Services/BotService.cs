﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ChatJobsity.Services
{
    public class BotService
    {
        public async Task<BotResponse> ExecuteCommand(string strCommand)
        {
            string[] arrCommand = strCommand.Split('=');
            if (arrCommand.Length != 2)
            {
                return new BotResponse { Success = false, Message = "Bad command" };
            }
            else
            {
                string commandCode = arrCommand[1];
                var builder = new UriBuilder("https://stooq.com/q/l/");
                var query = HttpUtility.ParseQueryString(builder.Query);
                query["s"] = commandCode;
                query["f"] = "sd2t2ohlcv";
                query["h"] = "";
                query["e"] = "csv";
                builder.Query = query.ToString();
                string url = builder.ToString();
                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        HttpResponseMessage response = await client.GetAsync(url);
                        HttpContent content = response.Content;
                        Stream stream = await content.ReadAsStreamAsync();
                        StreamReader reader = new StreamReader(stream);
                        reader.ReadLine();
                        String[] row = reader.ReadLine().ToString().Split(',');

                        if (row.Length > 0)
                        {
                            var responseBody = row[0] + " quote is $" + row[6] + " per share";
                            return new BotResponse { Success = true, Message = responseBody };
                        }
                        else
                        {
                            return new BotResponse { Success = false, Message = "Empty Service" };
                        }

                    }
                }
                catch (Exception e)
                {
                    return new BotResponse { Success = false, Message = e.Message + url };
                }
            }
        }
    }

    public class BotResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}