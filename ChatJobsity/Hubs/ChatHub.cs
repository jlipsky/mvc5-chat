﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ChatJobsity.DAL;
using ChatJobsity.Models;
using ChatJobsity.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace ChatJobsity.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {

        public async Task Send(int roomId, string message)
        {
            using (var db = new ChatJobsityDbContext())
            {
                string roomIdStr = roomId.ToString();
                string userId = Context.User.Identity.GetUserId();
                Participant participant = db.Participants.FirstOrDefault(x => x.RoomID == roomId && x.UserID == userId);
                RoomService rs = new RoomService(db);
                if (message[0] == '/')
                {
                    string dateStr = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    Clients.Group(roomIdStr).addChatMessage(dateStr, "Bot", "Executing command");

                    BotService bot = new BotService();
                    BotResponse r = await Task.Run(() => bot.ExecuteCommand(message));

                    dateStr = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    Clients.Group(roomIdStr).addChatMessage(dateStr, "Bot", r.Message);
                }
                else
                {
                    Message messageObj = rs.Message(roomId, participant, message);
                    Clients.Group(roomIdStr).addChatMessage(messageObj.CreatedPretty, participant.User.UserName, message);
                }
            }
        }

        public async Task JoinRoom(int roomId)
        {
            string roomIdStr = roomId.ToString();
            await Groups.Add(Context.ConnectionId, roomIdStr);
            Clients.Group(roomIdStr).addChatInfo(Context.User.Identity.Name, "joined.");
            await this.UpdateContactList(roomId);
        }

        public async Task UpdateContactList(int roomId)
        {
            using (var db = new ChatJobsityDbContext())
            {
                string roomIdStr = roomId.ToString();
                IEnumerable<String> participants = db.Participants.Where<Participant>(x => x.RoomID == roomId).Select(s => s.User.UserName).ToArray();
                _ = await Clients.Group(roomIdStr).UpdateContactList(participants);
            }
        }

        public async Task LeaveRoom(int roomId)
        {
            String roomIdStr = roomId.ToString();
            _ = Groups.Remove(Context.ConnectionId, roomIdStr);
            int count = RoomService.LeaveRoom(Context.User.Identity.GetUserId(), roomId);
            if (count > 0)
            {
                await UpdateContactList(roomId);
                Clients.Group(roomIdStr).addChatInfo(Context.User.Identity.Name, "left.");
            }

        }
    }
}