﻿using ChatJobsity.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChatJobsity.DAL
{
    public class ChatJobsityDbContext : IdentityDbContext<ApplicationUser>
    {
        public ChatJobsityDbContext(): base("ChatConnection", throwIfV1Schema: false) {}

        public static ChatJobsityDbContext Create()
        {
            return new ChatJobsityDbContext();
        }

        public DbSet<Room> Rooms { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Participant> Participants { get; set; }
    }
}